using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        [Key]
        public int Id { get; set; }
        public DateTime Data { get; set; }
        public EnumStatusPedido Status { get; set; }
        public Vendedor Vendedor { get; set; }
        public Produto Produtos { get; set; }
    }
}