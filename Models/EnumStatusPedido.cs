namespace tech_test_payment_api.Models
{
    public enum EnumStatusPedido
    {
        AguardandoPagamento,
        PagamentoAprovado,
        EnviadoParaTransportadora,
        Entregue,
        Cancelado
    }
}