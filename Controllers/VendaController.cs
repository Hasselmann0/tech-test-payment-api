using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;
using tech_test_payment_api.Context;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {

        private readonly VendaContext _context;

        public VendaController(VendaContext context)
        {
            _context = context;
        }


        [HttpGet("BuscarVenda")]
        public IActionResult BuscarVenda(int id, int IdVendedor, int IdProduto)
        {
            var venda = _context.Vendas.Find(id);
            var vendedor = _context.Vendedores.Find(IdVendedor);
            var produtos = _context.Produtos.Find(IdProduto);

            venda.Status = EnumStatusPedido.AguardandoPagamento;
            venda.Vendedor = vendedor;
            venda.Produtos = produtos;

            if (venda == null)
                return NotFound();

            return Ok(venda);
        }

        [HttpPost("VendaRealizada")]
        public IActionResult VendaRealizada(int IdVendedor, int IdProduto, Venda venda)
        {
            var vendedor = _context.Vendedores.Find(IdVendedor);
            var produtos = _context.Produtos.Find(IdProduto);


            venda.Status = EnumStatusPedido.AguardandoPagamento;
            venda.Vendedor = vendedor;
            venda.Produtos = produtos;
            venda.Data = DateTime.Now;


            _context.Add(venda);
            _context.SaveChanges();

            return CreatedAtAction(nameof(BuscarVenda), new { id = venda.Id }, venda);
        }

        [HttpPut("AtualizarVenda")]
        public IActionResult AtualizarVenda(int id, Venda venda)
        {
            var vendaBanco = _context.Vendas.Find(id);
            var _vendedor = _context.Vendedores.Find(id);
            var vendaProd = _context.Produtos.Find(id);

            venda.Data = DateTime.Now;
            venda.Produtos = vendaProd;
            venda.Vendedor = _vendedor;

            

            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();

            return Ok(vendaBanco);
        }
    }
}