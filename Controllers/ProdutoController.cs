using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProdutoController : ControllerBase
    {
        private readonly VendaContext _contex;

        public ProdutoController(VendaContext context)
        {
            _contex = context;
        }

        [HttpPost("CadastrarProdutoEstoque")]
        public IActionResult CadastrarProduto(Produto produto)
        {
            _contex.Add(produto);
            _contex.SaveChanges();

            return Ok(produto);
        }
    }
}