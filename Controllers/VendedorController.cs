using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly VendaContext _context;

        public VendedorController(VendaContext context)
        {
            _context = context;
        }

        [HttpPost("RegistrarVendedor")]
        public IActionResult RegistrarVendedor(Vendedor vendedor)
        {
            _context.Add(vendedor);
            _context.SaveChanges();
            return Ok(vendedor);
        }

        [HttpGet("BuscarVendedor{id}")]
        public IActionResult BuscarVendedorId(int id)
        {
            var vendedor = _context.Vendedores.Find(id);

            if (vendedor == null)
                return NotFound();

            return Ok(vendedor);
        }
    }
}